/*****************************************************************************
 * main.cpp
 *
 * Copyright 2018 Waleed Hamra. All rights reserved.
 *
 * This file may be distributed under the terms of GNU Public License version
 * 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
 * license should have been included with this file, or the project in which
 * this file belongs to. You may also find the details of GPL v3 at:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * If you have any questions regarding the use of this file, feel free to
 * contact the author of this file, or the owner of the project in which
 * this file belongs to.
*****************************************************************************/
#include <iostream>
#include <fstream>
#include <string>
#include <curl/curl.h>
#include <vector>


using namespace std;

string token;
string chatid;

string readStream(istream &in)
{
    string ret;
    char buffer[4096];
    while (in.read(buffer, sizeof(buffer)))
        ret.append(buffer, sizeof(buffer));
    ret.append(buffer, in.gcount());
    return ret;
}

CURLcode sendMsg(string msg)
{
    CURL *curl;
    curl = curl_easy_init();
    string url = "https://api.telegram.org/bot" + token + "/sendmessage";
    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        string postData = "text=" + string(curl_easy_escape(curl, msg.c_str(), 0));
        postData += "&chat_id=" + chatid;
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData.c_str());
        return curl_easy_perform(curl);
    }
    return CURLE_FAILED_INIT;
}

vector<string> neatSplit(string msg, unsigned int size)
{
    vector<string> msgList;
    while (msg.size() > size)
    {
        size_t marker = msg.find_last_of("\n", size);
        msgList.push_back(msg.substr(0, marker));
        msg = msg.substr(marker+1);
    }
    msgList.push_back(msg);
    return msgList;
}

int main()
{
    ifstream conf("/etc/telegram-send.conf", ifstream::in);
    string buffer;
    while (getline(conf, buffer))
    {
        string key = buffer.substr(0, buffer.find(" "));
        if (key == "token")
            token = buffer.substr(buffer.find("=")+2, buffer.size());
        else if (key == "chatid")
            chatid = buffer.substr(buffer.find("=")+2, buffer.size());
    }
    if (token.size() == 0)
    {
        cerr << "Missing token in configuration file!" << endl;
        return 1;
    }
    if (chatid.size() == 0)
    {
        cerr << "Missing Chat ID in configuration file!" << endl;
        return 1;
    }
    string msg = readStream(cin);
    vector<string> msgList = neatSplit(msg, 4096);
    for (unsigned int i = 0; i < msgList.size(); i++)
    {
        sendMsg(msgList.at(i));
    }
    return 0;
}
